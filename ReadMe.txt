Run npm Install
If breeze is installed - no need to install tailwind manually. If not:
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
Add the paths to all of your template files in your tailwind.config.js file. :
content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  Add the @tailwind directives for each of Tailwind’s layers to your ./resources/css/app.css file.
  @tailwind base;
  @tailwind components;
  @tailwind utilities;
- Install any missing nodes or modules.
- Run npm run dev.
- Run php artisan serve.
- Log in as one of the admins to gain access to the panel, on login route or on welcome view.
If you are importing the database doniraj_kompjuter.sql from the database folder, there is no need to run migrations, it's already set up and the user token for post requests is in database folder - token.txt file.
If you are creating a new database:
- After the first migration make sure to uncomment the user3 seed in the AdminSeeder
to create a new general purpose user for the front end and then run db:seed. 
- After seeding, uncomment the /token route in the API routes and the token method in the ApiController,
then go to the /api/token route to create a token for the general user so that the front end can send 
post requests using the token in the header in each request. Copy the plain text token and store it somewhere.
- All web routes protected with Auth.
- All API routes protected with sanctum.
- CRUD functionalities for all resources for extensive controll on the admin panel.
